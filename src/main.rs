use std::{fs, io::{stdin, stdout, BufRead, Write}, path::PathBuf, str::FromStr, thread, time::Duration};
use crossterm::{cursor, terminal, ExecutableCommand};
use clap::Parser;
use polyfunge::{enums::HaltType, ProgramState};

#[derive(Parser, Debug)]
#[command(about, long_about = None)]
struct Args {
    /// Path of the Polyfunge program to run
    path: PathBuf,

    /// Input buffer
    #[arg(long, short, default_value_t = String::default())]
    input: String,

    /// Runs without display, and ignores breakpoints
    #[arg(long, default_value_t = false)]
    headless: bool,

    /// Skips the command prompt and starts the program in run mode
    #[arg(short, long, default_value_t = false)]
    run: bool,

    /// Ticks per second
    #[arg(short, long, default_value_t = 10)]
    tps: u32,

    /// Goes fast
    #[arg(short, long, default_value_t = false)]
    gotta_go_fast: bool,
}

fn main() {
    let args = Args::parse();
    let input_string = args.input;
    let path = args.path;
    let source_code = fs::read_to_string(path)
        .expect("Couldn't open file");
    let duration = Duration::from_millis(((1.0 / args.tps as f32) * 1000.0) as u64);

    let mut state = polyfunge::parse(source_code.as_str());
    state.input_buffer = input_string;
    let (col, row) = (0, 0);

    let mut stdout = stdout();
    if !args.headless && !args.run {
        println!("Polyfunge v.1 Rust Interpreter (pfv.1ri)");
        println!();
        command_list();
        command_prompt(&mut state, &mut stdout);
    }

    let mut displayed_output_until = 0; // For headless

    while !state.is_program_halted() {
        state.tick();
        
        if !args.headless {
            let _ = stdout.execute(cursor::MoveTo(col, row));
            let _ = stdout.execute(terminal::Clear(terminal::ClearType::FromCursorDown));
            println!("{}", state.render_ascii_bg());
            state.prints_pointers_on_top_of_pre_rendered_bg(&mut stdout, col, row);
            println!("=======");
            println!("Tick  : {}", state.tick);
            println!("Output: {}", state.output_buffer);
            if state.stepping {
                if state.break_hit {
                    println!("Breakpoint!");
                }
                command_prompt(&mut state, &mut stdout);
            } else if !args.gotta_go_fast {
                thread::sleep(duration);
            }
        } else if state.output_buffer.len() > displayed_output_until {
            print!("{}", &state.output_buffer[displayed_output_until..state.output_buffer.len()]);
            let _ = stdout.flush();
            displayed_output_until = state.output_buffer.len();
        }
    }

    if args.headless {
        println!("output: {}", state.output_buffer);
    }

    match state.halt_type {
        Some(halt) => {
            let return_value_string = match state.return_value {Some(s) => s.to_string(), None => String::from_str("None").unwrap()};
            match halt {
                HaltType::Halted => {
                    println!("Program returned code `{}`", return_value_string);
                },
                HaltType::Error => {
                    println!("Error was triggered with code `{}`", return_value_string);
                }
                HaltType::Died => {
                    println!("Program died (no pointer left on grid");
                },
                HaltType::UserQuit => {
                    println!("Have a good day!")
                },
            }
        }
        None => {
            println!("Program halted for unknown reasons.");
        }
    }
}

fn command_prompt(state: &mut ProgramState, stdout: &mut std::io::Stdout) {
    let mut looping = true;
    while looping {
        print!("$> ");
        let _ = stdout.flush();
        let mut buf = String::new();
        let _ = stdin().lock().read_line(&mut buf);
        buf.pop();
        match buf.as_str() {
            "h" | "help" =>  {
                command_list();
            },
            "r" | "run" => {
                state.stepping = false;
                looping = false;
            },
            "s" | "step" => {
                state.stepping = true;
                looping = false;
            },
            "q" | "quit" => {
                state.halt_type = Some(HaltType::UserQuit);
                looping = false;
                },
            _ => {
                println!("{}: command not found. `h` for help", buf);
            }
        }
    }
}

fn command_list() {
    println!("Commands:");
    println!("`h`: displays this help");
    println!("`r`: starts the program in run mode");
    println!("`s`: steps one tick");
    println!("`q`: quits the program");
}