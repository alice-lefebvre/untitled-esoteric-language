use std::cmp::Ordering;
use std::io::Stdout;
use std::str::FromStr;
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use crossterm::style::{self, Color};
use crossterm::{cursor, ExecutableCommand};
use rand::Rng;
pub mod enums;
use crate::enums::*;
use crate::enums::Tile::*;
use crate::enums::Direction::*;

impl ProgramState {
    pub fn tick(&mut self) {
        // Every variable is added to the buckets, to help detect position. The key is the position on the grid (y*width + x)
        // and it contains a vector of (index, pointer). This informs us on what pointers have collided, and the index
        // is needed if we end up needing to delete them
        #[allow(clippy::type_complexity)]
        let mut buckets: HashMap<usize, Vec<(usize, Rc<RefCell<Pointer>>)>> = HashMap::new();
        let mut indices_to_delete = Vec::with_capacity(self.pointers.len());
        let mut variables_to_add = Vec::new();

        self.break_hit = false;

        // First pass: executing movements + putting new positions in buckets to facilitate collision detection
        for (i, p) in self.pointers.iter().enumerate() {
            
            let mut p_mut = p.borrow_mut();

            let index = self.fjdskfjdsk(p_mut.x, p_mut.y);
            let starting_tile = self.grid[index];

            let distance = if starting_tile == Jump {2} else {1};

            if !self.execute_move(&mut p_mut, distance) {
                indices_to_delete.push(true);
                continue;
            }

            let index = self.fjdskfjdsk(p_mut.x, p_mut.y);

            match buckets.get_mut(&index) {
                Some(vec) => {
                    vec.push((i, p.clone())); 
                },
                None => {
                    buckets.insert(index, vec![(i, p.clone())]);
                },
            }
            indices_to_delete.push(false); // we're safe... for now
        }

        // Combining pointers with same direction on the same tile
        // I already forgot how that works
        for (_key, value) in buckets.iter_mut() {
            let mut indices_to_delete_2 = vec![false; value.len()];
            if value.len() > 1 {
                for (i, v) in value.iter().enumerate() {
                    let current_pointer_to_compare_against = v.1.clone();
                    // if the pointer is up for deletion, we skip
                    if indices_to_delete_2[i] {
                        continue;
                    }

                    for (j, v) in value.iter().enumerate() {
                        if i == j {
                            continue;
                        }

                        let pointer = v.1.clone();

                        if pointer.borrow().direction == current_pointer_to_compare_against.borrow().direction {
                            indices_to_delete[v.0] = true;
                            indices_to_delete_2[j] = true;
                            current_pointer_to_compare_against.borrow_mut().value += pointer.borrow().value;
                            current_pointer_to_compare_against.borrow_mut().wait = false; // counter-intuitively, by doing that we ensure a wait if this spawns on a wait tile
                        }
                    }
                }        
                // Removing combined pointer from the bucket
                let mut iter = indices_to_delete_2.iter_mut();
                value.retain(|_| !*iter.next().unwrap());
            }
        }
    
        
        // Acting on collisions
        for (key, value) in buckets.iter() {
            let tile = self.grid[*key];
            match value.len().cmp(&2) {
                Ordering::Equal => {
                let mut p1 = value[0].1.borrow_mut();
                let mut p2 = value[1].1.borrow_mut();
                if p1.direction == p2.direction {
                    panic!("oops, not supposed to be possible");
                } else {
                    let explosion = ProgramState::handle_collision(&mut p1, &mut p2, tile);
                    if explosion {
                        for (i, _) in value {
                            indices_to_delete[*i] = true;
                        }
                    }
                }
            },
            Ordering::Greater => {
                // Allez hop ça dégage
                for (i, _) in value {
                    indices_to_delete[*i] = true;
                }
            },
            _ => {},
        }
        }

        // Removing deleted variables a first time here so that they don't trigger tile effects
        let mut iter = indices_to_delete.iter_mut();
        self.pointers.retain(|_| !*iter.next().unwrap());

        // Generic tile effects (one operand)
        let mut indices_to_delete = Vec::with_capacity(self.pointers.len());
        for p in self.pointers.iter() {
            let mut p_mut = p.borrow_mut();
            let index = p_mut.y * self.width + p_mut.x;
            let tile = self.grid[index];
            match tile {
                ConveyerUp => p_mut.direction = Direction::Up,
                ConveyerRight => p_mut.direction = Direction::Right,
                ConveyerDown => p_mut.direction = Direction::Down,
                ConveyerLeft => p_mut.direction = Direction::Left,
                Wait => p_mut.wait = !p_mut.wait,
                Spinner => {
                    let n_rotations = rand::thread_rng().gen_range(0..4);
                    for _ in 0..n_rotations {
                        p_mut.direction = p_mut.direction.clockwise();
                    }
                },
                VerticalGate => {
                    if !p_mut.direction.is_vertical() {
                        p_mut.direction = p_mut.direction.reverse();
                    }
                },
                HorizontalGate => {
                    if p_mut.direction.is_vertical() {
                        p_mut.direction = p_mut.direction.reverse();
                    }
                },
                Duplicate => {
                    if p_mut.direction.is_vertical() {
                        variables_to_add.push(Rc::new(RefCell::new(Pointer::new(p_mut.x, p_mut.y, p_mut.value, Left))));
                        variables_to_add.push(Rc::new(RefCell::new(Pointer::new(p_mut.x, p_mut.y, p_mut.value, Right))));
                    } else {
                        variables_to_add.push(Rc::new(RefCell::new(Pointer::new(p_mut.x, p_mut.y, p_mut.value, Down))));
                        variables_to_add.push(Rc::new(RefCell::new(Pointer::new(p_mut.x, p_mut.y, p_mut.value, Up))));
                    }
                },
                Destroy => {
                    indices_to_delete.push(true);
                    continue;
                },
                Test => {
                    if p_mut.value != 0 {
                        indices_to_delete.push(true);
                        continue;
                    }
                },
                Not => {
                    if p_mut.value == 0 {
                        indices_to_delete.push(true);
                        continue;
                    }
                },
                Zero => p_mut.value = 0,
                Increment => p_mut.value += 1,
                Decrement => p_mut.value -= 1,
                Input => {
                    if !self.input_buffer.is_empty() {
                        p_mut.value = self.input_buffer.remove(0) as i32;
                    } else {
                        p_mut.value = 0;
                    }
                },
                NumericalInput => {
                    if !self.input_buffer.is_empty() {
                        let input_buffer_copy = self.input_buffer.clone();
                        let split = input_buffer_copy.trim_start().split_once(' ');
                        p_mut.value = match split {
                            Some((word, remainder)) => {
                                self.input_buffer = String::from_str(remainder.trim_start()).unwrap();
                                i32::from_str(word).unwrap_or_default()
                            },
                            None => {0}
                        };
                    } else {
                        p_mut.value = 0;
                    }
                },
                Breakpoint => {
                    self.break_hit = true;
                    self.stepping = true;
                },
                Print => {
                    self.output_buffer.push_str(p_mut.value.to_string().as_str());
                    self.output_buffer.push(' ');
                },
                PrintASCII => {
                    self.output_buffer.push(char::from_u32(p_mut.value as u32).unwrap());
                }
                Error => {
                    self.output_buffer.push_str(format!("Error triggered with value {}", p_mut.value).as_str());
                    self.halt_type = Some(HaltType::Error);
                    self.return_value = Some(p_mut.value);
                }
                Halt => {
                    if !self.is_program_halted() {
                        self.return_value = Some(p_mut.value);
                        self.halt_type = Some(HaltType::Halted);
                    }
                }
                _ => () 
            } 
            indices_to_delete.push(false);
        }

        // deleting again
        let mut iter = indices_to_delete.iter_mut();
        self.pointers.retain(|_| !*iter.next().unwrap());

        // Adding new stuff
        self.pointers.append(&mut variables_to_add);

        // Check for end of program
        if self.pointers.is_empty() {
            self.halt_type = Some(HaltType::Died);
        }

        self.tick += 1;
    }

    /// Returns still alive
    pub fn execute_move(&self, p_mut: &mut Pointer, distance: i32) -> bool {
        if !p_mut.wait {
            let mut y = p_mut.y as i32;
            let mut x = p_mut.x as i32;
            match p_mut.direction {
                Direction::Up => y -= distance,
                Direction::Right => x += distance,
                Direction::Down => y += distance,
                Direction::Left => x -= distance,
            }
            if x < 0 || x >= self.width as i32 || y < 0 || y >= self.height as i32 {
                return false;
            }
            p_mut.x = x as usize;
            p_mut.y = y as usize;
        }
        true
    }

    /// Handles collision, returns true if there's an explosion
    /// Does not handle the special case of fusion
    pub fn handle_collision(p1: &mut Pointer, p2: &mut Pointer, tile: Tile) -> bool {
        match tile {
            Add => {
                p1.value += p2.value;
                p2.value = p1.value;
            },
            Sub => {
                let old_v = p1.value;
                p1.value -= p2.value;
                p2.value -= old_v;
            },
            Multiply => {
                p1.value *= p2.value;
                p2.value = p1.value;
            },
            Divide => {
                if p1.value == 0 || p2.value == 0 {
                    return true;
                }
                let old_v = p1.value;
                p1.value /= p2.value;
                p2.value /= old_v;
            },
            Modulo => {
                let old_v = p1.value;
                p1.value %= p2.value;
                p2.value %= old_v;
            }
            GreaterThan => {
                let old_v = p1.value;
                p1.value = if p1.value > p2.value {0} else {1};
                p2.value = if p2.value > old_v {0} else {1};
            },
            LessThan => {
                let old_v = p1.value;
                p1.value = if p1.value < p2.value {0} else {1};
                p2.value = if p2.value < old_v {0} else {1};
            },
            VerticalGate => {
                if (p1.direction.is_vertical()) && (!p2.direction.is_vertical()) {
                    if p1.value == 0 {
                        // this looks like this is the opposite of what we want, and that's true. But in the main tick() function,
                        // when a value encounters the reflector in the reflecting direction, it gets automatically reflected.
                        // By reversing the reverse, we allow the value to pass through when the signal is 0
                        p2.direction = p2.direction.reverse();
                    }
                } else if (p2.direction.is_vertical()) && (!p1.direction.is_vertical()) {
                    if p2.value == 0 {
                        p1.direction = p1.direction.reverse();
                    }
                } else {
                    return true;
                }
            }
            HorizontalGate => {
                if (!p1.direction.is_vertical()) && (p2.direction.is_vertical()) {
                    if p1.value == 0 {
                        // see above
                        p2.direction = p2.direction.reverse();
                    }
                } else if (!p2.direction.is_vertical()) && (p1.direction.is_vertical()) {
                    if p2.value == 0 {
                        p1.direction = p1.direction.reverse();
                    }
                } else {
                    return true;
                }
            }
            _ => {return true;}
        }
        false
    }

    pub fn fjdskfjdsk(&self, x: usize, y: usize) -> usize {
        y * self.width + x
    }

    pub fn render_ascii_bg(&self) -> String {
        let mut string = String::new();
        for j in 0..self.height {
            for i in 0..self.width {
                let tile = self.grid[self.fjdskfjdsk(i, j)];
                let char = tile.to_char();
                string.push(char);
            }
            string.push('\n');
        };
        string
    }

    pub fn dirty_final_render(&self) -> String {
        unsafe {
            let mut bg = self.render_ascii_bg();
            let string = bg.as_mut_str().as_bytes_mut();
        

            for v in self.pointers.iter() {
                let v = (*v).borrow();
                string[v.x + v.y * (self.width +1)] = ((v.value % 10) + 48) as u8;
            }

            String::from_utf8_unchecked(string.to_vec())
        }
    }

    pub fn prints_pointers_on_top_of_pre_rendered_bg(&self, stdout: &mut Stdout, col: u16, row: u16) {
        for p in self.pointers.iter() {
            let _ = stdout.execute(cursor::MoveTo(col, row));
            let p = p.borrow();
            let _ = stdout.execute(cursor::MoveToRow(row + p.y as u16));
            let _ = stdout.execute(cursor::MoveToColumn(col + p.x as u16));
            let (color, text) = match p.value.cmp(&0) {
                Ordering::Greater => (Color::DarkYellow, format!("{}", (p.value % 10))),
                Ordering::Equal => (Color::Green, format!("{}", 0)),
                Ordering::Less => (Color::Red, format!("{}", (-p.value % 10))),
            };
            let _ = stdout.execute(style::SetForegroundColor(color));
            print!("{text}");
            let _ = stdout.execute(style::ResetColor);
        }
        let _ = stdout.execute(cursor::MoveTo(col, row + self.height as u16 + 1));
    }
}

pub fn parse(input: &str) -> ProgramState {
    let mut width: usize = 0;
    let mut height: usize = 0;
    for l in input.lines() {
        height += 1;
        if l.len() > width {
            width = l.len();
        }
    }

    let mut grid = Vec::new();
    let mut pointers = Vec::new();

    for (j, l) in input.lines().enumerate() {
        let mut evaluating_numeral = false;
        let mut evaluating_char = false;
        let mut numeral_starting_pos = (0, 0);
        let mut numeral_builder = String::new();
        let mut v = vec![Tile::Empty; width];
        for (i, c) in l.chars().enumerate() {
            if evaluating_numeral {
                if c.is_ascii_digit() || c == '-' {
                    numeral_builder.push(c);
                    if i == l.len() - 1 {
                        evaluating_numeral = false;
                        let value = numeral_builder.as_str().parse().unwrap_or_else(|_| panic!("Unable to parse {}", numeral_builder));
                        pointers.push(Rc::new(RefCell::new(Pointer::new(numeral_starting_pos.0, numeral_starting_pos.1, value, Down))));
                    }
                    continue;
                } else {
                    evaluating_numeral = false;
                    let value = numeral_builder.as_str().parse().unwrap_or_else(|_| panic!("Unable to parse {}", numeral_builder));
                    pointers.push(Rc::new(RefCell::new(Pointer::new(numeral_starting_pos.0, numeral_starting_pos.1, value, Down))));
                }
            }
            if evaluating_char {
                evaluating_char = false;
                pointers.push(Rc::new(RefCell::new(Pointer::new(i - 1, j, c as i32, Down))));
                continue;
            }
            
            if c == '(' {
                evaluating_numeral = true;
                numeral_starting_pos = (i, j);
                numeral_builder = String::new();
            } else if c == '\'' {
                evaluating_char = true;
            } else if c.is_ascii_digit() {
                pointers.push(Rc::new(RefCell::new(Pointer::new(i, j, c as i32 - 48, Down))));
            } else {
                v[i] = Tile::from_char(&c); 
            }
        }
        grid.append(&mut v);
    }
    ProgramState::new(grid, width, height, pointers)
}

pub struct Pointer {
    pub x: usize,
    pub y: usize,
    pub value: i32,
    pub direction: Direction,
    wait: bool,
}

pub struct ProgramState {
    pub grid: Vec<Tile>,
    pub width: usize,
    pub height: usize,
    pub pointers: Vec<Rc<RefCell<Pointer>>>,
    pub output_buffer: String,
    pub input_buffer: String,
    pub halt_type: Option<HaltType>,
    pub stepping: bool,
    pub break_hit: bool,
    pub return_value: Option<i32>,
    pub tick: u32,
}

impl ProgramState {
    pub fn new(grid: Vec<Tile>, width: usize, height: usize, pointers: Vec<Rc<RefCell<Pointer>>>) -> ProgramState {
        ProgramState{
            grid,
            width,
            height,
            pointers,
            output_buffer: String::new(),
            input_buffer: String::new(),
            halt_type: None,
            stepping: false,
            break_hit: false,
            return_value: None,
            tick: 0}
    }

    pub fn is_error(&self) -> bool {
        match self.halt_type {
            Some(h) => {
                h == HaltType::Error
            },
            None => {
                false
            },
        }
    }
    
    pub fn is_program_halted(&self) -> bool {
        self.halt_type.is_some()
    }
}

impl Pointer {
    pub fn new(x: usize, y:usize, value: i32, direction: Direction) -> Pointer {
        Pointer { x, y, value, direction, wait: false }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn pointer(value: i32) -> Pointer {
        Pointer::new(0, 0, value, Down)
    }

    #[test]
    pub fn test_collision_division() {
        let mut a = pointer(3);
        let mut b = pointer(6);
        let mut c = pointer(6);
        let mut zero = pointer(0);

        let destruction = ProgramState::handle_collision(&mut a, &mut b, Divide);
        assert_eq!(a.value, 0);
        assert_eq!(b.value, 2);
        assert!(!destruction);
        let destruction = ProgramState::handle_collision(&mut c, &mut zero, Divide);
        assert!(destruction);
        // actually, what do we want?
    }
}