use Tile::*;
use Direction::*;

impl Tile {
    pub fn from_char(c: &char) -> Tile {
        match c {
            '^' => ConveyerUp,
            '>' => ConveyerRight,
            'v' => ConveyerDown,
            '<' => ConveyerLeft,
            'w' => Wait,
            '#' => Jump,
            '_' => HorizontalGate,
            '|' => VerticalGate,
            '?' => Test,
            '!' => Not,
            '@' => Spinner,

            '+' => Add,
            '-' => Sub,
            '*' => Multiply,
            '/' => Divide,
            '%' => Modulo,
            'i' => Increment,
            'd' => Decrement,
            'g' => GreaterThan,
            'l' => LessThan,

            ':' => Duplicate,
            'x' => Destroy,
            'I' => Input,
            'N' => NumericalInput,
            'z' => Zero,
            'p' => Print,
            'P' => PrintASCII,
            'b' => Breakpoint,
            'e' => Error,
            'h' => Halt,

            ' ' => Empty,
            _ => Unkown(*c),
        }
    }

    pub fn to_char(&self) -> char {
        match self {
            ConveyerUp => '^',
            ConveyerRight => '>',
            ConveyerDown => 'v',
            ConveyerLeft => '<',
            Wait => 'w',
            Jump => '#',
            HorizontalGate => '_',
            VerticalGate => '|',
            Test => '?',
            Not => '!',
            Spinner => '@',

            Add => '+',
            Sub => '-',
            Multiply => '*',
            Divide => '/',
            Modulo => '%',
            Increment => 'i',
            Decrement => 'd',
            GreaterThan => 'g',
            LessThan => 'l',
            And => '&',
            
            Duplicate => ':',
            Destroy => 'x',
            Input => 'I',
            NumericalInput => 'N',
            Zero => 'z',
            Print => 'p',
            PrintASCII => 'P',
            Breakpoint => 'b',
            Error => 'e',
            Halt => 'h',

            Empty => ' ',
            Unkown(c) => *c,
        }
    }
}

impl Direction {
    pub fn reverse(&self) -> Direction {
        match self {
            Right => Left,
            Left => Right, // me when i become a millionaire
            Up => Down, // funk you up
            Down => Up,
        }
    }

    pub fn clockwise(&self) -> Direction {
        match self {
           Up => Right, // bass
           Right => Down,
           Down => Left,
           Left => Up, 
        }
    }

    pub fn counterclockwise(&self) -> Direction {
        self.clockwise().clockwise().clockwise()
    }

    pub fn is_vertical(&self) -> bool {
        *self == Up || *self == Down
    }
}
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum HaltType {
    Halted,
    Error,
    Died,
    UserQuit,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Tile {
    Empty,
    // ctrl flow
    ConveyerUp,
    ConveyerRight,
    ConveyerDown,
    ConveyerLeft,
    Jump,
    VerticalGate,
    HorizontalGate,
    Test,
    Not,
    Wait,
    Spinner,
    // ALU
    Add,
    Sub,
    Multiply,
    Divide,
    Modulo,
    Increment,
    Decrement,
    GreaterThan,
    LessThan,
    And,
    // UTIL
    Duplicate,
    Zero,
    Destroy,
    Input,
    NumericalInput,
    Print,
    PrintASCII,
    Breakpoint,
    Error,
    Halt,
    Unkown(char),
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_tiles_conversion() {
        let starter_str = "+-/*%gl:zxIpPhbe";
        let mut finished_string = String::new();
        let mut intermediate = Vec::new();
        for c in starter_str.chars() {
            intermediate.push(Tile::from_char(&c));
        }
        for t in intermediate {
            finished_string.push(t.to_char());
        }
        assert_eq!(starter_str, finished_string.as_str());

    }
}