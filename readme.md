# Polyfunge

Polyfunge is an esoteric language inspired by [Befunge](https://esolangs.org/wiki/Befunge). As a fungeoid, it has familiar qualities: programs are an ascii grid, the `^`, `>`, `v`, `<` operators that change the direction of an instruction pointer, recognizable tiles (`#`, `|`, etc.).

It is, however, quite different. There is no stack, and no singular instruction pointer/program counter. Instead, any value is its own instruction pointer (referred to as simply pointers), that will behave a lot like a regular Befunge instruction pointer. What makes this language fun (hopefully to you too) is managing several pointers executing instructions independentely, and having to occasionally smash pointers together to make math or logic happen (or, just for the fun of it).

## Table Of Contents

[TOC]

## General stuff
### The Grid

A Polyfunge program is defined by a 2-dimensional grid of tiles and an array of pointers that are moving on that grid. Computation happen when pointers encounter tiles or interact with each others in ways that will be descibed later.

The starting state of a program will be represented by ASCII characters here. However, an ASCII reprensentation of the program does obscure some information about the current state once the execution has begun. For instance, you cannot know where to pointers are moving towards, and pointers could hide tiles.

### Pointers

Pointers are the central element of the language. They have a value and a direction (they're pointing somewhere if you will). A pointer will move one space by tick in that direction.

They are spawned by simply typing a numeral. Due to gravity, their initial direction is down. Consider the following sample program:

```
3


h
```

This 3 will spawn a pointer (3;down) before the execution starts. When that happens, it will immediately start moving down. It will eventually encounter the `h` tile (for **halt**), which will end the program.

To inputs numbers larger than 9, there exist some syntaxic sugar. use `(` followed by a decimal expansion to enter any number. Before execution, the number will be put in a pointer at the position of the parenthesis. The same trick exists for character with `'` : a pointer will be created with the ASCII code of that character for value.

```
(17 'a
h   h
```

Pointers whose value are greater than 9 may be represented by their last digit for readability.

Without pointers, nothing can happen, so when there are none left at any given point, the program is halted. Pointers also get destroyed when they reach the edges of the grid.

## The Laws of Motion

### Conveyor belts

Remember how that `3` from earlier was just falling forever ? Thanks to conveyers, we can have it not do that. When a pointer encounters a conveyer, its direction changes accordingly. Consider the following program, in which our pointer is trapped in a fun loop:

```
3
>  v

^  <
```

`>`, `v`, `<` and `^` are used to change the direction of any variable that runs into that space, in the way that you would think that they do. The pointer occupies that space for a tick as its direction is updated.


### Jumps & timing

If you need your pointer to go over some traffic, the **jump** tile (`#`) is there for you:

```
     h
3    h
>   #h    p
     h
     h
```

Our brave 3 jumps over a wall of `halt`s and manages to print itself (`p`).

Timing is very important in Polyfunge (see the section on [math](#Math)). jump tiles can also be used to accelerate movement. However if you need your pointer to slow down, a **wait** tile (`w`) will make it wait on that space for one tick.

### Collision behaviour

You might wonder what happens when two pointer end in the same space. Here's an explanatory illustration:

```
a -> boom
      ^
      |
      b
```

Both pointers get annihilated **before** triggering any potential tiles.

There are some exceptions though, some tiles, such as mathematical operation or gates **expect** you to collide two pointer on their space.

Note how in the following example these two variables are never in the same space at the same time, so a collision will never happen (they will just keep swapping places).

```
3  3
>  <
```

This is the principle that allows for this super fast spawner to exist:

```
  3
3x
>:<
 
 x
```

There is an exception though: if two pointers end up in the same space, with the same direction, something beautiful happens: they join their forces and become a new pointer whose value is the sum of its two creators. This happens before any tile effect is treated.

```
  1
1
>#>p
```

This will output `2`. The joining of forces, or fusion of pointers, happen before collisions are checked for. (This only matter for very niche edge cases)

## Arithmetics & Logic

### Math

Mathematical operations are carried by having two pointers on a math tile during the same tick. Let's see how we would divide two values for insance:

```
  6

3
> /

```

To read the results, let's fast forward to one tick after the operation:

```


> /0
  2
```

The pointers continue to move in their original direction, but their values have been altered to reflect the result of the mathematical operation. Notice that the pointers have different values . This is because the pointer with value 6 has been modified to represent the result of 6/3, and the pointer of value 3 has been modified to represent the result of 3/6. In general, this is how operations are carried:

```
     b
     |
     v
a -> x -> axb
     |
     v
    bxa
```

Where x here represents the operation attributed to a mathematical tile, and a and b are pointers.

Note that the pointers can come from opposite directions as well, though this makes for trickier code:

```
 6     3
v># / #<v
>           // 3/6
        >   // 6/3
```

If a lone pointer passes through a math tile, nothing happens.

### Some logic

A pointer with value 0 is used by logic tiles as a logical **true**. The absence of any pointer, or a pointer that carries any other value may be interpreted as a logical **false**.

The **zero** tile (`z`) is really useful to send logic signals.

#### Compare

Greater than (`g`) and less than (`l`) are two-operator logic tiles, that return `0` if `a` is strictly greater than `b`, and if `a` is strictly less than `b` respectively.

Testing for equality can be achieved with the substraction (`-`) tile, that will output `0` if the two values are equal.

#### Gates tiles

Gates is how you do a lot of branching in Polyfunge. They will either allow or deny passage to any pointer in a certain axis, depending on what value they receive from the other axis.

For instance, the vertical gate (`|`) reflects pointers moving in an horizontal axis (left or right), unless it receives a pointer of value 0 from the top or bottom **during the same tick**. Example: this 3 is trapped (it will get reflected back and forth between `>` and `|`) until the 0 comes to free it.

```
            0
 v          <
3
>| h
```

`_` is the horizontal equivalent to that.

Here is a way to get an if-else kind of statement: if the pointer is a 0 (as it is in the example), our `3` will go to the if branch. But if it is anything else, it'll find itself in the else branch.

```
    0

 3
 >#v|  // if
   >   // else
```

#### Test tiles

Another way to do branching is with the **test** tile (`?`), and its evil counterpart the **not** tile (`!`). These will let pass specific pointer values and destroy the pointers that don't match the condition. **test** lets pointers with value 0 pass, while **not** lets pointers with every value except 0 pass.

The following example shows how to use test with a comparison tile:

```
  b
a
> g? // pointer of value 0 there if a > b, otherwise no pointer.
  x
```

## Utility

### Duplicate & Delete

The **duplicate** tile (`:`) will emit copies of a pointer that passes through it in the orthogonal direction, like so (this is a representation, not a valid program):

```
     a
     ^
     |
a -> : -> a
     |
     v
     a
```
The **delete** tile (`x`) is a great way to get rid of unwanted duplicates.

### I/O
`p` prints the decimal representation of a pointer, `P` prints its ASCII representation.

When a pointer triggers the ASCII input (`I`) tile, the first character of the input buffer gets popped and its value gets assigned to the pointer. If the input buffer is empty, sets the pointer to `0`.

The numerical input (`N`) works in a similar way, but parses the next word in the input buffer for a numerical integer value, and consumes it as well as surrounding spaces. The result is also assigned to the pointer. if the word is not an integer, it is not consummed, and the pointer is set to 0.

### Program termination

A Polyfunge program can terminate in 3 ways:

- If no pointers are left on the grid, in which case the return value is **nothing** (and not 0!)
- If a pointer hits a **halt** tile, in which case the return value is the value of the pointer that triggered the halt
- If a pointer hits an **error** tile, in which cas the return value is the value of the pointer that triggered the error, and the user gets informed that an error happened.

## List of commands

### Flow control

|block|name|description|
|----|-----|---|
| ^, >, v, < |conveyer| changex direction |
| w | wait|holds variable for one tick |
| # | jump|jump over one space |
| \| | vertical gate | horizontal passthrough if gets **true** from top or bottom, otherwise reflect | 
| _ | horizontal gate | vertical passthrough if gets **true** from left or right, otherwise reflect |
| ? | test | allows passage if value = 0, otherwise destroys it|
| ! | not | allows passage if value != 0, otherwise destroys it |
| @ | spinner wheel | sends incoming pointers to random directions |

### Arithmetic & Logic

|block|name   |description
|---|---|---|
| + | add | - |
| - | subtract | - |
| / | divide | integer division |
| * | multiply | - |
| % |modulo| - |
| i | increment | - |
| d | decrement | - |
| g | greater than | pointer value set to 0 if > to other pointer, 1 otherwise |
| l | less than | pointer value set to 0 if < to other pointer, 1 otherwise |

### Utility

|block| name | description    |
|---|---|---|
| : | duplicate | emits copies of the value in orthogonal directions |
| z | zero | sets the value of any pointer that passes on it to 0 |
| x | destroy | destroys any pointer |
| I | input | changes any pointer to the ASCII code of the next character in the input buffer. 0 if buffer is empty |
| N | numerical input | parse the next word in the input buffer as in integer, and changes any pointer to that value |
| p | print | prints the decimal value of a pointer |
| P | print ASCII | prints the ASCII value of a pointer | 
| h | halt | terminates execution |
| b | breakpoint | pauses execution into step mode |
| e | error | terminates execution and display the value of the pointer that triggered the error |

## Sample programs

### Hello world
```
'!'d'l'r'o'w' ','o'l'l'e'H
> > > > > > > > > > > > > P
```

### Mystery program

```
1
w
vx# #<
::zv
pw  
 w   #  
 0x
 >:< #
> +  ^
```

You can try to figure out what this program computes as a puzzle !

## Implementations

### This Rust interpreter

This Rust interpreter is to be considered as the reference implementation. You may want to steal the tests to try on your own implementation!

### Haskell??

For some reason, a really cool Haskell implementation is being developped [here](https://github.com/gpluscb/Polyfunge). I think it's pretty neat.

### Implementation specifications

- order of evaluating is 1. move 2. resolve collisions(annihilation & math & logic) 3. resolve tile effects (except for math & logic tiles)
- wrong configuration on gate tiles results in annihilation.
- dividing by 0 causes both pointers to be annihilated
- if pointer fusion happen on a wait tile and one of them has already spent a tick waiting, the resulting pointer always waits one tick anyway
- The polyfunge Fundation's (pff) recommended file extension is `.pff`, for "polyfunge file". For test files, use `.pft`, for "polyfunge test"