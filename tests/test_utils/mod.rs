use polyfunge::*;

/// Returns false if error, true if halted safely
pub fn runs_without_erroring(state: &mut ProgramState) -> bool {
    while !state.is_program_halted() {
        state.tick();
        if state.tick > 10000 {
            panic!("stuck");
        }
    }
    !state.is_error()
}