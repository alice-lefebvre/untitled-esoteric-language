use std::cell::RefCell;
use std::rc::Rc;

use polyfunge::*;
use polyfunge::enums::Tile::*;
use polyfunge::enums::Direction::*;

use crate::test_utils::runs_without_erroring;

mod test_utils;
//  6
// 3+
//
// 6 is moving down, 3 to the right
// This is a pain but we can now which vector is which for later idk maybe we could have parsed
fn setup_simple_math() -> ProgramState {
    let grid = vec![Empty, Empty, Empty,
                   Empty, Add  , Empty,
                   Empty, Empty, Empty];
    let pointers =  vec![Rc::new(RefCell::new(Pointer::new(1, 0, 6, Down))), Rc::new(RefCell::new(Pointer::new(0, 1, 3, Right)))];
    ProgramState::new(grid, 3, 3, pointers)
}

#[test]
pub fn addition() {
    let mut state = setup_simple_math();
    state.tick();
    state.tick();
    assert_eq!(state.pointers[0].borrow().value, 9);
    assert_eq!(state.pointers[1].borrow().value, 9);
}

#[test]
pub fn one_plus_one_equals_two() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("i\n");
    input.push_str("h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(2));
}

#[test]
pub fn one_minus_one_equals_zero() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("d\n");
    input.push_str("h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn substraction() {
    let mut state = setup_simple_math();
    state.grid[4] = Sub;
    state.tick();
    state.tick();
    assert_eq!(state.pointers[0].borrow().value, 3);
    assert_eq!(state.pointers[1].borrow().value, -3);
}

#[test]
pub fn mult() {
    let mut state = setup_simple_math();
    state.grid[4] = Multiply;
    state.tick();
    state.tick();
    assert_eq!(state.pointers[0].borrow().value, 18);
    assert_eq!(state.pointers[1].borrow().value, 18);
}

#[test]
pub fn divide() {
    let mut state = setup_simple_math();
    state.grid[4] = Divide;
    state.tick();
    state.tick();
    assert_eq!(state.pointers[0].borrow().value, 2);
    assert_eq!(state.pointers[1].borrow().value, 0);
    let mut state = setup_simple_math();
    state.grid[4] = Divide;
    state.pointers[0].borrow_mut().value = 0;
    state.tick();
    assert!(state.pointers.is_empty());
}

#[test]
pub fn modulo() {
    let mut state = setup_simple_math();
    state.grid[4] = Modulo;
    state.pointers[0].borrow_mut().value = 5;
    state.tick();
    state.tick();
    assert_eq!(state.pointers[0].borrow().value, 2);
    assert_eq!(state.pointers[1].borrow().value, 3);
}