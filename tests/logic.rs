use polyfunge::*;
use test_utils::runs_without_erroring;
mod test_utils;

#[test]
pub fn vertical_gate_refects_on_no_signal() {
    let mut input = String::new();
    input.push_str(" 1       \n");
    input.push_str("h># |e   \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn vertical_gate_refects_on_false() {
    let mut input = String::new();
    input.push_str("         \n");
    input.push_str(" 1  ><   \n");
    input.push_str("h># |1e   \n");
    input.push_str("    ^<   \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.pointers.len(), 2);
}

#[test]
pub fn vertical_gate_allows_on_true() {
    let mut input = String::new();
    input.push_str("    0   \n");
    input.push_str(" 1  w   \n");
    input.push_str("e># |h   \n");
    input.push_str("    ><");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.pointers.len(), 2);
}

#[test]
pub fn vertical_gate_resists_to_weird_configuration() {
    let mut input = String::new();
    input.push_str(" 1     1\n");
    input.push_str("e># | #<e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.pointers.len(), 0);
}

#[test]
pub fn vertical_gate_allows_vertical_passthrough() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("|\n");
    input.push_str("h\n");
    let mut input2 = String::new();
    input2.push_str("0\n");
    input2.push_str("|\n");
    input2.push_str("h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(1));
    let mut state = parse(input2.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn horizontal_gate_refects_on_no_signal() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("_\n");
    input.push_str("e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn horizontal_gate_refects_on_false() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str(" 1\n");
    input.push_str("_<\n");
    input.push_str("e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn horizontal_gate_allows_on_true() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str(" 0\n");
    input.push_str("_<\n");
    input.push_str("h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(1));
}

#[test]
pub fn horizontal_gate_resists_to_weird_configuration() {
    let mut input = String::new();
    input.push_str("e1\n");
    input.push_str("v<\n");
    input.push_str("#\n");
    input.push_str(" \n");
    input.push_str("_\n");
    input.push_str(" \n");
    input.push_str("#1\n");
    input.push_str("^<\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.pointers.len(), 0);
}

#[test]
pub fn horizontal_gate_allows_vertical_passthrough() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str(">_h\n");
    let mut input2 = String::new();
    input2.push_str("0\n");
    input2.push_str(">_h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(1));
    let mut state = parse(input2.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn test_allows_zero_to_pass() {
    let mut input = String::new();
    input.push_str(" 0\n");
    input.push_str(" ? \n");
    input.push_str(" h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn test_blocks_false() {
    let mut input = String::new();
    input.push_str(" 3\n");
    input.push_str(" ?\n");
    input.push_str(" e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn not_allows_false() {
    let mut input = String::new();
    input.push_str(" 3\n");
    input.push_str(" !\n");
    input.push_str(" h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(3));
}

#[test]
pub fn not_blocks_true() {
    let mut input = String::new();
    input.push_str(" 0\n");
    input.push_str(" !\n");
    input.push_str(" e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}