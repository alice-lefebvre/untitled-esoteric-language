use polyfunge::*;
use test_utils::runs_without_erroring;
mod test_utils;

#[test]
pub fn standard_collisions() {
    let mut state = parse("00 \n w \n>  ");
    state.tick();
    state.tick();
    state.tick();
    assert!(state.pointers.is_empty());
}

#[test]
pub fn fusion() {
    let mut input = String::new();
    input.push_str("  2  \n");
    input.push_str("1    \n");
    input.push_str(">#>  \n");
    let mut state = parse(input.as_str());
    for _ in 0..3 {
        state.tick();
    }
    assert_eq!(state.pointers.len(), 1);
    assert_eq!(state.pointers[0].borrow().value, 3);
}

#[test]
pub fn fusion_and_collision() {
    let mut input = String::new();
    input.push_str("  2 3\n");
    input.push_str("1    \n");
    input.push_str(">#>e<\n");
    let mut state = parse(input.as_str());
    for _i in 0..3 {
        state.tick();
    }
    assert!(state.is_program_halted());
    assert!(!state.is_error());
}

#[test]
pub fn fusion_and_math_operation() {
    let mut input = String::new();
    input.push_str("  11 \n");
    input.push_str("1  w \n");
    input.push_str(">#>+h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(3));
}

#[test]
pub fn pointers_created_by_fusion_should_always_wait_on_wait_tile() {
    let mut input = String::new();
    input.push_str("     \n");
    input.push_str("1 2  \n");
    input.push_str(">#>wh\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.tick, 5);
    assert_eq!(state.return_value, Some(3));
    let mut input = String::new();
    input.push_str("  2  \n");
    input.push_str("1    \n");
    input.push_str(">#>wh\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.tick, 5);
    assert_eq!(state.return_value, Some(3));
}

#[test]
pub fn double_fusion() {
    let mut input = String::new();
    input.push_str("  1  \n");
    input.push_str("1 1  \n");
    input.push_str(">#>wh\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(3));
}

#[test]
pub fn collisions_happen_before_tile_eval() {
    let mut input = String::new();
    input.push_str("  0 0 \n");
    input.push_str("1 >h< \n");
    input.push_str(">    h\n");
    let mut state = parse(input.as_str());
    state.tick();
    state.tick();
    assert_eq!(state.pointers.len(), 1);
    assert!(!state.is_program_halted());
}

#[test]
pub fn triple_collision() {
    let mut input = String::new();
    input.push_str("   0 \n");
    input.push_str("  0 0 \n");
    input.push_str("  >e< \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn conveyors_convey() {
    let mut input = String::new();
    input.push_str("3\n");
    input.push_str(">    v\n");
    input.push_str("h\n");
    input.push_str("^    <\n");
    let mut state = parse(input.as_str());
    runs_without_erroring(&mut state);
    assert_eq!(state.tick, 14);
}

#[test]
pub fn collision_over_destroy_doesnt_crash_the_program() {
    let mut input = String::new();
    input.push_str("0 0\n");
    input.push_str(">x<\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn destroy_works() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("x\n");
    input.push_str("e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}

#[test]
pub fn fusion_modify_behaviour_of_logic_tile() {
    let mut input = String::new();
    input.push_str("  1\n");
    input.push_str("(-1\n");
    input.push_str(">#>?h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn two_operator_tile_doesnt_allow_three_pointers() {
    let mut input = String::new();
    input.push_str(" 1\n");
    input.push_str("1 1\n");
    input.push_str(">+<\n");
    input.push_str(" e\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.halt_type, Some(enums::HaltType::Died));
}

#[test]
pub fn black_hole() {
    let mut input = String::new();
    input.push_str("   1    \n");
    input.push_str("  1w1   \n");
    input.push_str("   v    \n");
    input.push_str("1  #  1 \n");
    input.push_str("w  v:xw \n");
    input.push_str(">#>e<#< \n");
    input.push_str(" 1>^1   \n");
    input.push_str(" >^#    \n");
    input.push_str("   ^<   \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
}