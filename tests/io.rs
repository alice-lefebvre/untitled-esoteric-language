use std::str::FromStr;

use polyfunge::*;
use test_utils::runs_without_erroring;
mod test_utils;

#[test]
pub fn parse_and_print() {
    let mut input = String::new();
    input.push_str("'H (-17\n");
    input.push_str("P\n");
    input.push_str("   h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.output_buffer, "H");
    assert_eq!(state.return_value, Some(-17));
}

#[test]
pub fn character_input() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("I\n");
    input.push_str("P\n");
    let mut state = parse(input.as_str());
    state.input_buffer = String::from_str("Hello world").unwrap();
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.output_buffer, "H");
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("I\n");
    input.push_str("h\n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(0));
}

#[test]
pub fn numerical_input() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("N\n");
    input.push_str("h\n");
    let mut state = parse(input.as_str());
    state.input_buffer = String::from_str(" -26   hello").unwrap();
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(-26));
    assert_eq!(state.input_buffer, "hello");
}