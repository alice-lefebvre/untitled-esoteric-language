use polyfunge::*;

// we cast a big net by running a complex program that uses lots of mechanisms

static FIB: &str =
"
1
w
vx# #<
::zv
pw  
 w   #  
 0x
 >:< #
> +  ^
";
static PRIMES: &str = 
"
               1          2
             1x         2x
             >:<        >:<
     2      > +         >+        v
   2x         x          x
   >:<    5      > # # # # # v
>   +     v           > _x
    x    x:z^  v  #w <z   
          v    # #  x?^ w           x
^  p   < x:      :   gx      >    % ?v
      x  x_www < x      #         w x
^ ww  |^# <          #            w 
               #  x >*x       x   w
      ^   w w w: ww   # #     :  # # < 
3              x    #w            w
                    ^:x  x        w 
2                    ^   : < x|v# <
                           ^   < 
p                             x 
x                       #
                        ^<
";

#[test]
#[ignore]
pub fn compute_primes() {
    let mut state = parse(PRIMES);
    while !state.is_program_halted() && state.tick < 10000 {
        state.tick();
    }
    assert_eq!(state.output_buffer, "2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 ");
}

#[test]
#[ignore]
pub fn compute_fibonacci() {
    let mut state = parse(FIB);
    while !state.is_program_halted() && state.tick < 200 {
        state.tick();
    }
    assert_eq!(state.output_buffer, "1 1 2 3 5 8 13 21 34 55 89 ");
}