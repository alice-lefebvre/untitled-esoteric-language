use polyfunge::{enums::HaltType, *};

use crate::test_utils::runs_without_erroring;
mod test_utils;

#[test]
pub fn stop_when_no_pointers() {
    let mut input = String::new();
    input.push_str("1  \n");
    input.push_str(">  \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.tick, 4);
    assert_eq!(state.return_value, None);
    assert_eq!(state.halt_type, Some(HaltType::Died));
}
#[test]
pub fn halt_works() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("h\n");
    input.push_str(" \n");
    let mut state = parse(input.as_str());
    assert!(runs_without_erroring(&mut state));
    assert_eq!(state.tick, 1);
    assert_eq!(state.return_value, Some(1));
    assert_eq!(state.halt_type, Some(HaltType::Halted));
}

#[test]
pub fn error_works() {
    let mut input = String::new();
    input.push_str("1\n");
    input.push_str("e\n");
    input.push_str(" \n");
    let mut state = parse(input.as_str());
    assert!(!runs_without_erroring(&mut state));
    assert_eq!(state.tick, 1);
    assert_eq!(state.return_value, Some(1));
    assert_eq!(state.halt_type, Some(HaltType::Error));
}

#[test]
pub fn error_has_priority() {
    let mut input = String::new();
    input.push_str("21\n");
    input.push_str("eh\n");
    input.push_str("  \n");
    let mut state = parse(input.as_str());
    assert!(!runs_without_erroring(&mut state));
    assert_eq!(state.return_value, Some(2));
    assert_eq!(state.halt_type, Some(HaltType::Error));
}
